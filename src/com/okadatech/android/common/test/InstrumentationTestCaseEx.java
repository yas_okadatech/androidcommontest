package com.okadatech.android.common.test;

import android.content.Context;
import android.test.InstrumentationTestCase;

public class InstrumentationTestCaseEx extends InstrumentationTestCase {

    protected Context getTargetContext() {
        return getInstrumentation().getTargetContext();
    }
    
    protected Context getTestPackageContext() {
        return getInstrumentation().getContext();
    }
}
