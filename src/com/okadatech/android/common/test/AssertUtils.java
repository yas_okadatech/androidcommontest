package com.okadatech.android.common.test;

import android.graphics.Bitmap;

import junit.framework.Assert;

import java.nio.ByteBuffer;

public final class AssertUtils {

    private AssertUtils() { }
    
    public static void assertEquals(final Bitmap expected, final Bitmap actual) {
        final ByteBuffer bufExpected = ByteBuffer.allocate(expected.getByteCount());
        expected.copyPixelsToBuffer(bufExpected);
        final ByteBuffer bufActual = ByteBuffer.allocate(actual.getByteCount());
        actual.copyPixelsToBuffer(bufActual);
        Assert.assertEquals(0, bufExpected.compareTo(bufActual));
    }
}
