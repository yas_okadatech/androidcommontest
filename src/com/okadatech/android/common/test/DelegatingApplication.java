package com.okadatech.android.common.test;

import android.app.Application;
import android.content.Context;
import android.content.ContentProvider;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.test.RenamingDelegatingContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * This is a class which delegates to the given context, but performs database
 * and file operations with a renamed database/file name (prefixes default
 * names with a given prefix).
 */
public class DelegatingApplication extends Application {

    private RenamingDelegatingContext mDelegateContext;

    public static <T extends ContentProvider> T providerWithRenamedContext(
            Class<T> contentProvider, Context c, String filePrefix)
            throws IllegalAccessException, InstantiationException {
        return providerWithRenamedContext(contentProvider, c, filePrefix, false);
    }

    public static <T extends ContentProvider> T providerWithRenamedContext(
            Class<T> contentProvider, Context c, String filePrefix,
            boolean allowAccessToExistingFilesAndDbs)
            throws IllegalAccessException, InstantiationException {
        Class<T> mProviderClass = contentProvider;
        T mProvider = mProviderClass.newInstance();
        DelegatingApplication context = new DelegatingApplication(c, filePrefix);
        if (allowAccessToExistingFilesAndDbs) {
            context.makeExistingFilesAndDbsAccessible();
        }
        mProvider.attachInfo(context, null);
        return mProvider;
    }

    /**
     * Makes accessible all files and databases whose names match the filePrefix that was passed to
     * the constructor. Normally only files and databases that were created through this context are
     * accessible.
     */
    public void makeExistingFilesAndDbsAccessible() {
        mDelegateContext.makeExistingFilesAndDbsAccessible();
    }

    /**
     * @param context : the context that will be delagated.
     * @param filePrefix : a prefix with which database and file names will be
     * prefixed.
     */
    public DelegatingApplication(Context context, String filePrefix) {
        super();
        mDelegateContext = new RenamingDelegatingContext(context, filePrefix);
    }

    /**
     * @param context : the context that will be delagated.
     * @param fileContext : the context that file and db methods will be delgated to
     * @param filePrefix : a prefix with which database and file names will be
     * prefixed.
     */
    public DelegatingApplication(Context context, Context fileContext, String filePrefix) {
        super();
        mDelegateContext = new RenamingDelegatingContext(context, fileContext, filePrefix);
    }

    public String getDatabasePrefix() {
        return mDelegateContext.getDatabasePrefix();
    }

    @Override
    public SQLiteDatabase openOrCreateDatabase(String name,
            int mode, SQLiteDatabase.CursorFactory factory) {
        return mDelegateContext.openOrCreateDatabase(name, mode, factory);
    }

    @Override
    public SQLiteDatabase openOrCreateDatabase(String name,
            int mode, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        return mDelegateContext.openOrCreateDatabase(name, mode, factory, errorHandler);
    }

    @Override
    public boolean deleteDatabase(String name) {
        return mDelegateContext.deleteDatabase(name);
    }
    
    @Override
    public File getDatabasePath(String name) {
        return mDelegateContext.getDatabasePath(name);
    }

    @Override
    public String[] databaseList() {
        return mDelegateContext.databaseList();
    }

    @Override
    public FileInputStream openFileInput(String name)
            throws FileNotFoundException {
        return mDelegateContext.openFileInput(name);
    }

    @Override
    public FileOutputStream openFileOutput(String name, int mode)
            throws FileNotFoundException {
        return mDelegateContext.openFileOutput(name, mode);
    }

    @Override
    public File getFileStreamPath(String name) {
        return mDelegateContext.getFileStreamPath(name);
    }

    @Override
    public boolean deleteFile(String name) {
        return mDelegateContext.deleteFile(name);
    }

    @Override
    public String[] fileList() {
        return mDelegateContext.fileList();
    }
    
    /**
     * In order to support calls to getCacheDir(), we create a temp cache dir (inside the real
     * one) and return it instead.  This code is basically getCacheDir(), except it uses the real
     * cache dir as the parent directory and creates a test cache dir inside that.
     */
    @Override
    public File getCacheDir() {
        return mDelegateContext.getCacheDir();
    }

    @Override
    public Context getApplicationContext() {
        return this;
    }

    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        return mDelegateContext.getSharedPreferences(name, mode);
    }

    @Override
    public Resources getResources() {
        return mDelegateContext.getResources();
    }
    

//    /**
//     * Given an array of files returns only those whose names indicate that they belong to this
//     * context.
//     * @param allFiles the original list of files
//     * @return the pruned list of files
//     */
//    private String[] prunedFileList(String[] allFiles) {
//        List<String> files = Lists.newArrayList();
//        for (String file : allFiles) {
//            if (file.startsWith(mFilePrefix)) {
//                files.add(file);
//            }
//        }
//        return files.toArray(new String[]{});
//    }
}
