package com.okadatech.android.common.test;

import android.content.Context;
import android.content.SharedPreferences;

public class DelegatingApplicationTest extends InstrumentationTestCaseEx {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

 
    public void testGetApplicationContext() {
        DelegatingApplication target = new DelegatingApplication(getTargetContext(), "ut");
        assertEquals(target, target.getApplicationContext());
    }
    
    public void testGetSharedPreferences() {
        DelegatingApplication target = new DelegatingApplication(getTargetContext(), "ut");
        SharedPreferences pref = target.getSharedPreferences("", Context.MODE_PRIVATE);
        assertNotNull(pref);
    }
    
    public void testGetResources() {
        DelegatingApplication target = new DelegatingApplication(getTargetContext(), "ut");
        assertNotNull(target.getResources());
    }
}
