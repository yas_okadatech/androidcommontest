package com.okadatech.android.common.test;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.okadatech.android.common.test.test.R;

public class AssertUtilsTest extends InstrumentationTestCaseEx {
    
    private Context mContext;

    protected void setUp() throws Exception {
        super.setUp();
        
        mContext = getTestPackageContext();
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testAssertEquals() {
        final Bitmap expected = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.assert_expected);
        final Bitmap actual = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.assert_test);
        AssertUtils.assertEquals(expected, actual);
    }
}
